$(document).ready(function(){
	function response(dataType) {
		$.ajax({
			url : '/demo/users/'+$.mydata.userId,
			dataType : dataType,
			type : 'GET'
		}).done(function(data){
			$('#response').text(dataType == 'json' ? JSON.stringify(data, null, '\t') : (new XMLSerializer()).serializeToString(data));
		});
	}

	$('#get-user-xml-button').click(function(event){
		response('xml');
		event.preventDefault();
	});
	
	$('#get-user-json-button').click(function(event){
		response('json');
		event.preventDefault();
	});
});