package com.home.web.controller;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.home.core.domain.User;
import com.home.service.UserService;
import com.home.web.domain.Delete;
import com.home.web.domain.Users;
import com.home.web.domain.messeges.FieldMessage;
import com.home.web.domain.messeges.FieldValidationMessage;
import com.home.web.domain.messeges.Message;
import com.home.web.domain.messeges.Message.MESSAGE_TYPE;
import com.home.web.domain.messeges.Messages;

@Controller
@RequestMapping(value = "users")
public class UserController {

	@Resource
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET, value = "post")
	public String get(@ModelAttribute("user") User user, ModelMap modelMap) {
		/**
		 * When I try to send the redirectAttribte with
		 * BindingResult.MODEL_KEY_PREFIX+"user" as the key, my binding gets
		 * erased. This is only way I can figure out how to do this, and will
		 * change it if I ever see a better way.
		 */
		modelMap.put(BindingResult.MODEL_KEY_PREFIX + "user",
				modelMap.get("errors"));
		return "users/post";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String post(@Valid @ModelAttribute("user") User user,
			BindingResult errors, RedirectAttributes redirectAttributes) {
		if (errors.hasErrors()) {
			redirectAttributes.addFlashAttribute("user", user);
			redirectAttributes.addFlashAttribute("errors", errors);
			return "redirect:post";
		}
		userService.put(user);
		redirectAttributes.addAttribute("userId", user.getUserId());
		return "redirect:{userId}";
	}

	@RequestMapping(method = RequestMethod.DELETE, consumes = {
			"application/json", "application/xml", "text/xml" }, produces = {
			"application/json", "application/xml", "text/xml" })
	public @ResponseBody
	void delete(@Valid @RequestBody Delete deleteUsers) {
		userService.delete(deleteUsers.getIds());
	}

	@RequestMapping(value = "{userId}", method = RequestMethod.DELETE)
	public @ResponseBody
	void delete(@PathVariable String userId) {
		userService.delete(userId);
	}

	@RequestMapping(method = RequestMethod.GET, produces = { "text/html" })
	public String viewAll(@RequestParam(required = false) String search,
			ModelMap modelMap) {

		modelMap.put("users", userService.fetchLike(search));
		modelMap.put("search", search);
		return "users/users";
	}

	@RequestMapping(method = RequestMethod.GET, produces = {
			"application/json", "application/xml", "text/xml" })
	public @ResponseBody
	Users viewAll(@RequestParam(required = false) String search) {
		return new Users(userService.fetchLike(search));
	}

	@RequestMapping(value = "{userId}", method = RequestMethod.GET, produces = "text/html")
	public String view(@PathVariable String userId, ModelMap modelMap) {
		modelMap.put("user", userService.fetch(userId));
		return "users/user";
	}

	@RequestMapping(value = "{userId}", method = RequestMethod.GET, produces = {
			"application/json", "application/xml", "text/xml" })
	public @ResponseBody
	User view(@PathVariable String userId) {
		return userService.fetch(userId);
	}

	@RequestMapping(value = "modify", method = RequestMethod.GET)
	public void modify() {
		// return "users/modify";
	}

	@RequestMapping(value = "error", method = RequestMethod.GET)
	public void error() throws Exception {
		throw new Exception("derp");
	}

	@RequestMapping(method = RequestMethod.PUT, consumes = {
			"application/json", "application/xml", "text/xml" })
	public @ResponseBody
	Messages put(@Valid @RequestBody Users users) {
		Collection<Message> messages = new ArrayList<Message>();
		for (String message : userService.put(users.getUser()))
			messages.add(new Message(MESSAGE_TYPE.INFO, message));
		return new Messages(messages);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody
	FieldValidationMessage handleMethodArgumentNotValidException(
			MethodArgumentNotValidException exception) {
		Collection<FieldMessage> fieldMessage = new ArrayList<FieldMessage>();
		for (FieldError error : exception.getBindingResult().getFieldErrors()) {
			fieldMessage.add(new FieldMessage(error.getField(), error
					.getDefaultMessage()));
		}
		return new FieldValidationMessage(fieldMessage, new Message(
				MESSAGE_TYPE.VALIDATION, "Please check fields for errors."));
	}

	/**
	 * We are going to return 404 when AccessDeniedException is thrown from a
	 * method. This lets makes sure that the person accessing the data is not
	 * told that it actually exists with out the proper access
	 * 
	 * @return
	 */
	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String accessDenied() {
		return "error/404";
	}

}
