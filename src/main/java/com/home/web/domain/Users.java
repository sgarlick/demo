package com.home.web.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

import com.home.core.domain.User;

@XmlRootElement
public class Users {

	public Users() {
		user = new ArrayList<User>();
	}

	public Users(Collection<User> users) {
		user = users;
	}

	@NotEmpty
	@Valid
	private Collection<User> user;

	public Collection<User> getUser() {
		return user;
	}

	public void setUser(Collection<User> user) {
		this.user = user;
	}
}
