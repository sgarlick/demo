package com.home.web.domain.messeges;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Message {
	public enum MESSAGE_TYPE { VALIDATION, INFO };
	private MESSAGE_TYPE type;
	private String message;
	public Message()
	{
		type = MESSAGE_TYPE.INFO;
	}


	public Message(MESSAGE_TYPE type, String message) {
		super();
		this.type = type;
		this.message = message;
	}


	public MESSAGE_TYPE getType() {
		return type;
	}
	
	public void setType(MESSAGE_TYPE type) {
		this.type = type;
	}


	public String getMessage() {
		return message;
	}

	@XmlAttribute
	public void setMessage(String message) {
		this.message = message;
	}

}
