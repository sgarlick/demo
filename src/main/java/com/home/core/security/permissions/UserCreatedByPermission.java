package com.home.core.security.permissions;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.security.core.Authentication;

public class UserCreatedByPermission implements Permission {

	private final NamedParameterJdbcOperations namedParameterJdbcOperations;

	public UserCreatedByPermission(
			NamedParameterJdbcOperations namedParameterJdbcOperations) {
		super();
		this.namedParameterJdbcOperations = namedParameterJdbcOperations;
	}

	@Override
	public boolean isAllowed(Authentication authentication, Object targetObject) {
		if (null == authentication || !authentication.isAuthenticated()
				|| !(targetObject instanceof String)) {
			return false;
		}

		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("userId", targetObject.toString());
		paramMap.put("auth", authentication.getName());
		try {
			namedParameterJdbcOperations
					.queryForInt(
							"select 1 from t_users where user_id = :userId and created_by = :auth",
							paramMap);
		} catch (EmptyResultDataAccessException e) {
			return false;
		}
		return true;

	}

	public NamedParameterJdbcOperations getNamedParameterJdbcOperations() {
		return namedParameterJdbcOperations;
	}

}
